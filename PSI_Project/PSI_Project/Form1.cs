﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Imaging.Filters;
using System.Drawing.Imaging;
using AForge.Neuro;
using AForge.Neuro.Learning;
using System.IO;
using System.Threading;

namespace PSI_Project
{
    public partial class Form1 : Form
    {

        ActivationNetwork neuro;
        double[][] input_ldata;
        bool draw = false;
        int pX = -1;
        int pY = -1;
        Bitmap image;
        Graphics g;
        Pen pen;
        double[] handWData;
        bool NNStatus = false;
        int[] input_ldata_sizes = new int[36]; //used for learning many folders at once


        public Form1()
        {
            neuro = new ActivationNetwork(new SigmoidFunction(), 400, 36); // check function modifier (alpha)

            InitializeComponent();
            comboBox1.SelectedIndex = 0;
            image = new Bitmap(panel1.Width, panel1.Height, PixelFormat.Format24bppRgb);
            panel1.CreateGraphics();
            panel2.CreateGraphics();
            Graphics.FromImage(image).Clear(Color.White);
            g = Graphics.FromImage(image);

            pen = new Pen(Color.Black, 10);
            pen.StartCap = LineCap.Round;
            pen.EndCap = LineCap.Round;
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Left))
                draw = true;
            else if (e.Button.Equals(MouseButtons.Right))
            {
                Graphics.FromImage(image).Clear(Color.White);

                panel1.Invalidate();
                panel2.Invalidate();
            }

        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Left))
            {
                draw = false;

                Bitmap tmp;
                tmp = cropAndMinimalize(image);
                panel2.CreateGraphics().DrawImage(tmp, 0, 0, 50, 50);
                handWData = makeNGreyscale(tmp);
                setLabelsScales(neuro.Compute(handWData));
            }
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (draw)
            {
                g.DrawLine(pen, pX, pY, e.X, e.Y);
                panel1.CreateGraphics().DrawImageUnscaled(image, new Point(0, 0));
            }

            pX = e.X;
            pY = e.Y;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImageUnscaled(image, new Point(0, 0));
        }

        // Output cropping - size : 20x20
        public Bitmap cropAndMinimalize(Bitmap img)
        {

            Invert filter1 = new Invert();
            ExtractBiggestBlob filter2 = new ExtractBiggestBlob();
            ResizeBicubic filter3 = new ResizeBicubic(20, 20);

            Bitmap tmp, tmp1, tmpE;
            tmp1 = img.Clone(new Rectangle(0, 0, img.Width, img.Height), PixelFormat.Format24bppRgb);
            tmpE = tmp1;
            try
            {
                filter1.ApplyInPlace(tmp1);
                tmp = filter2.Apply(tmp1);
                filter1.ApplyInPlace(tmp);
                tmp1 = filter3.Apply(tmp);
            }
            catch (System.ArgumentException e)
            {
                return tmpE;
            }
            return tmp1;
        }

        //Converting to normalized greyscale tab (0.3+0.59+0.11)
        public double[] makeNGreyscale(Bitmap img)
        {
            double[] imgtab = new double[img.Height * img.Width];
            for (int i = 0; i < img.Height; i++)
                for (int j = 0; j < img.Width; j++)
                {
                    Color oc = img.GetPixel(i, j);
                    imgtab[i * img.Width + j] = ((((oc.R * 0.3) + (oc.G * 0.59) + (oc.B * 0.11)) - 127.5) / 127.5) * (-1.0);
                }
            return imgtab;
        }

        public void network()
        {


        }

        // Start/Stop Button
        private void button1_Click(object sender, EventArgs e)
        {
            if (NNStatus == true)
            {
                button1.Text = "Start NNetwork";
                label78.Text = "Stopped";
                pictureBox1.Enabled = false;
                button4.Enabled = false;
                panel1.Enabled = false;

                button2.Enabled = true;
                button3.Enabled = true;
                button5.Enabled = true;
                button6.Enabled = true;
                comboBox1.Enabled = true;

                Graphics.FromImage(image).Clear(Color.White);
                panel1.Invalidate();

                NNStatus = false;
            }
            else
            {
                button1.Text = "Stop NNetwork";
                label78.Text = "Working";
                panel1.Enabled = true;
                pictureBox1.Enabled = true;
                button4.Enabled = true;

                button2.Enabled = false;
                button3.Enabled = false;
                button5.Enabled = false;
                button6.Enabled = false;
                comboBox1.Enabled = false;

                NNStatus = true;
            }
        }

        // Load Button
        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Open Neural Network File";
            dialog.Filter = "Neural Network File|*.nnf";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                neuro = (ActivationNetwork)Network.Load(dialog.FileName);
            }
            pictureBox4.Visible = true;
            pictureBox4.Refresh();
            Thread.Sleep(1000);
            pictureBox4.Visible = false;
        }

        // Save Button
        private void button3_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "Save Neural Network File";
            dialog.FileName = "neural_network.nnf";
            dialog.Filter = "Neural Network File|*.nnf";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                neuro.Save(dialog.FileName);
            }
            pictureBox5.Visible = true;
            pictureBox5.Refresh();
            Thread.Sleep(1000);
            pictureBox5.Visible = false;

        }

        // Load Image Button
        private void button4_Click(object sender, EventArgs e)
        {
            Bitmap tmp, tmp1;

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Open handwritten image file";
            dialog.Filter = "|*.png";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                tmp = new Bitmap(dialog.FileName.ToString());
                tmp1 = cropAndMinimalize(tmp);
                panel2.CreateGraphics().DrawImage(tmp1, 0, 0, 50, 50);
                handWData = makeNGreyscale(tmp1);
                setLabelsScales(neuro.Compute(handWData));
            }
        }

        // Load learn folder
        private void button5_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 36)
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                String[] files;
                Bitmap tmp, tmp1;

                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    if (!String.IsNullOrWhiteSpace(fbd.SelectedPath))
                        files = Directory.GetFiles(fbd.SelectedPath);
                    else files = null;

                    input_ldata = new double[files.GetLength(0)][];

                    for (int i = 0; i < files.GetLength(0); i++)
                    {
                        tmp = new Bitmap(files[i]);
                        tmp1 = cropAndMinimalize(tmp);
                        input_ldata[i] = makeNGreyscale(tmp1);
                    }
                    pictureBox2.Visible = true;
                }
            }
            else
            {
                OpenFileDialog dialog = new OpenFileDialog();
                String[] files;
                String[] dirs = new String[36];
                Bitmap tmp, tmp1;
                dialog.Title = "Open learning file";
                dialog.Filter = "|*.lf";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    StreamReader reader = new StreamReader(dialog.FileName);
                    for (int i = 0; i < 36; i++)
                    {
                        dirs[i] = reader.ReadLine();
                        files = Directory.GetFiles(dirs[i]);
                        input_ldata_sizes[i] = files.GetLength(0);
                    }
                    reader.Close();
                    int size = 0;
                    for (int i = 0; i < 36; i++)
                    {
                        size += input_ldata_sizes[i];
                    }
                    input_ldata = new double[size][];
                    for (int i = 0; i < 36; i++)
                    {
                        files = Directory.GetFiles(dirs[i]);
                        for (int j = 0; j < input_ldata_sizes[i]; j++)
                        {
                            tmp = new Bitmap(files[j]);
                            tmp1 = cropAndMinimalize(tmp);
                            input_ldata[i * input_ldata_sizes[i] + j] = makeNGreyscale(tmp1);
                        }
                    }
                    pictureBox2.Visible = true;
                }
            }
        }

        // Start learning
        private void button6_Click(object sender, EventArgs e)
        {
            if (pictureBox2.Visible == false)
                return;

            double[][] output_ldata = new double[input_ldata.GetLength(0)][];

            if (comboBox1.SelectedIndex != 36)
            {
                for (int i = 0; i < output_ldata.GetLength(0); i++)
                {
                    output_ldata[i] = new double[36];
                    for (int j = 0; j < 36; j++)
                    {
                        if (j == comboBox1.SelectedIndex)
                        {
                            output_ldata[i][j] = 1;
                        }
                        else
                        {
                            output_ldata[i][j] = 0;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < 36; i++)
                {
                    for (int j = 0; j < input_ldata_sizes[i]; j++)
                    {
                        output_ldata[i * input_ldata_sizes[i] + j] = new double[36];
                        for (int k = 0; k < 36; k++)
                        {
                            if (k == i)
                            {
                                output_ldata[i * input_ldata_sizes[i] + j][k] = 1;
                            }
                            else
                            {
                                output_ldata[i * input_ldata_sizes[i] + j][k] = 0;
                            }
                        }
                    }
                }
            }
            PerceptronLearning teacher = new PerceptronLearning(neuro);
            double error;
            double error_t = Double.Parse(textBox1.Text);
            do
            {
                error = teacher.RunEpoch(input_ldata, output_ldata);
            } while (error > error_t);
            pictureBox3.Visible = true;
            pictureBox3.Refresh();
            Thread.Sleep(1000);
            pictureBox2.Visible = false;
            pictureBox3.Visible = false;
        }

        public void setLabelsScales(double[] tab)
        {
            label37.Text = (((tab[0] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label38.Text = (((tab[1] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label39.Text = (((tab[2] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label40.Text = (((tab[3] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label41.Text = (((tab[4] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label42.Text = (((tab[5] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label43.Text = (((tab[6] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label44.Text = (((tab[7] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label45.Text = (((tab[8] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label46.Text = (((tab[9] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label47.Text = (((tab[10] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label48.Text = (((tab[11] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label49.Text = (((tab[12] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label50.Text = (((tab[13] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label51.Text = (((tab[14] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label52.Text = (((tab[15] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label53.Text = (((tab[16] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label54.Text = (((tab[17] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label55.Text = (((tab[18] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label56.Text = (((tab[19] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label57.Text = (((tab[20] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label58.Text = (((tab[21] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label59.Text = (((tab[22] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label60.Text = (((tab[23] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label61.Text = (((tab[24] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label62.Text = (((tab[25] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label63.Text = (((tab[26] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label64.Text = (((tab[27] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label65.Text = (((tab[28] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label66.Text = (((tab[29] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label67.Text = (((tab[30] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label68.Text = (((tab[31] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label69.Text = (((tab[32] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label70.Text = (((tab[33] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label71.Text = (((tab[34] * 100.0).ToString() + ' ').Substring(0, 4) + "%");
            label72.Text = (((tab[35] * 100.0).ToString() + ' ').Substring(0, 4) + "%");

            double max = 0;
            int index = 0;

            for (int i = 0; i < 36; i++)
            {
                if (max < tab[i])
                {
                    max = tab[i];
                    index = i;
                }
            }

            label37.ForeColor = Color.Black;
            label38.ForeColor = Color.Black;
            label39.ForeColor = Color.Black;
            label40.ForeColor = Color.Black;
            label41.ForeColor = Color.Black;
            label42.ForeColor = Color.Black;
            label43.ForeColor = Color.Black;
            label44.ForeColor = Color.Black;
            label45.ForeColor = Color.Black;
            label46.ForeColor = Color.Black;
            label47.ForeColor = Color.Black;
            label48.ForeColor = Color.Black;
            label49.ForeColor = Color.Black;
            label50.ForeColor = Color.Black;
            label51.ForeColor = Color.Black;
            label52.ForeColor = Color.Black;
            label53.ForeColor = Color.Black;
            label54.ForeColor = Color.Black;
            label55.ForeColor = Color.Black;
            label56.ForeColor = Color.Black;
            label57.ForeColor = Color.Black;
            label58.ForeColor = Color.Black;
            label59.ForeColor = Color.Black;
            label60.ForeColor = Color.Black;
            label61.ForeColor = Color.Black;
            label62.ForeColor = Color.Black;
            label63.ForeColor = Color.Black;
            label64.ForeColor = Color.Black;
            label65.ForeColor = Color.Black;
            label66.ForeColor = Color.Black;
            label67.ForeColor = Color.Black;
            label68.ForeColor = Color.Black;
            label69.ForeColor = Color.Black;
            label70.ForeColor = Color.Black;
            label71.ForeColor = Color.Black;
            label72.ForeColor = Color.Black;

            switch (index)
            {
                case 0:
                    label37.ForeColor = Color.Red;
                    break;
                case 1:
                    label38.ForeColor = Color.Red;
                    break;
                case 2:
                    label39.ForeColor = Color.Red;
                    break;
                case 3:
                    label40.ForeColor = Color.Red;
                    break;
                case 4:
                    label41.ForeColor = Color.Red;
                    break;
                case 5:
                    label42.ForeColor = Color.Red;
                    break;
                case 6:
                    label43.ForeColor = Color.Red;
                    break;
                case 7:
                    label44.ForeColor = Color.Red;
                    break;
                case 8:
                    label45.ForeColor = Color.Red;
                    break;
                case 9:
                    label46.ForeColor = Color.Red;
                    break;
                case 10:
                    label47.ForeColor = Color.Red;
                    break;
                case 11:
                    label48.ForeColor = Color.Red;
                    break;
                case 12:
                    label49.ForeColor = Color.Red;
                    break;
                case 13:
                    label50.ForeColor = Color.Red;
                    break;
                case 14:
                    label51.ForeColor = Color.Red;
                    break;
                case 15:
                    label52.ForeColor = Color.Red;
                    break;
                case 16:
                    label53.ForeColor = Color.Red;
                    break;
                case 17:
                    label54.ForeColor = Color.Red;
                    break;
                case 18:
                    label55.ForeColor = Color.Red;
                    break;
                case 19:
                    label56.ForeColor = Color.Red;
                    break;
                case 20:
                    label57.ForeColor = Color.Red;
                    break;
                case 21:
                    label58.ForeColor = Color.Red;
                    break;
                case 22:
                    label59.ForeColor = Color.Red;
                    break;
                case 23:
                    label60.ForeColor = Color.Red;
                    break;
                case 24:
                    label61.ForeColor = Color.Red;
                    break;
                case 25:
                    label62.ForeColor = Color.Red;
                    break;
                case 26:
                    label63.ForeColor = Color.Red;
                    break;
                case 27:
                    label64.ForeColor = Color.Red;
                    break;
                case 28:
                    label65.ForeColor = Color.Red;
                    break;
                case 29:
                    label66.ForeColor = Color.Red;
                    break;
                case 30:
                    label67.ForeColor = Color.Red;
                    break;
                case 31:
                    label68.ForeColor = Color.Red;
                    break;
                case 32:
                    label69.ForeColor = Color.Red;
                    break;
                case 33:
                    label70.ForeColor = Color.Red;
                    break;
                case 34:
                    label71.ForeColor = Color.Red;
                    break;
                case 35:
                    label72.ForeColor = Color.Red;
                    break;

            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            double bin;
            if (!Double.TryParse(textBox1.Text, out bin))
            {

                textBox1.ForeColor = Color.Red;
                textBox1.Refresh();
                Thread.Sleep(300);
                textBox1.ForeColor = Color.Black;
                textBox1.Text = "0,01";
                textBox1.Refresh();
            }
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 36)
            {
                label81.Text = "Choose file for learn:";
                button5.Text = "Load all";
            }
            else
            {
                label81.Text = "Choose folder for learn:";
                button5.Text = "Load";
            }
        }
    }
}
