import java.util.ArrayList;
import java.util.List;

public class Neuron {
    private List<Double> Inputs;
    private List<Double> Scales;
    private int default_function;

    public Neuron(int size, int default_function) {
        Inputs = new ArrayList<>(size);
        Scales = new ArrayList<>(size);
        addInputs(size);
        this.default_function = default_function;
    }

    public void addInputs(int size) {
        for (int i = 0; i < size; i++) {
            Inputs.add(0d);
            Scales.add(0.5);
        }
    }

    public double processNode(int index) {
        return (Inputs.get(index) * Scales.get(index));
    }

    public double activatingFuction(double neuronPotential) {
        switch (default_function) {
            //linear
            case 0: {
                if (neuronPotential > 0)
                    return 1;
                else if (neuronPotential < 0)
                    return -1;
                else
                    return 0;
            }
            //sigmoid !B=1
            case 1:
            default: {
                return 1 / (1 + Math.pow(Math.E, -1 * neuronPotential));
            }
        }
    }

    public double getNeuronPotential() {
        if (getInputsSize() == 0)
            return -1;
        double sum = 0;
        for (int i = 0; i < getInputsSize(); i++) {
            sum += processNode(i);
        }
        return sum;
    }

    public double getNeuronOutput() {
        if (getInputsSize() == 0)
            return -1;
        return activatingFuction(getNeuronPotential());
    }

    public int getInputsSize() {
        return Inputs.size();
    }

    public void setInputsValue(int index, double value) {
        Inputs.set(index, value);
    }

    public double getInputsValue(int index) {
        return Inputs.get(index);
    }

    public void setScalesValue(int index, double value) {
        Scales.set(index, value);
    }

    public double getScaleValue(int index) {
        return Scales.get(index);
    }


}
