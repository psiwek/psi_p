public class PSI {
    public static Neuron neu;
    public static void main(String args[]){
        neu = new Neuron(2,0);
        // AND
        neu.setScalesValue(0,1);
        neu.setScalesValue(1,1);
        System.out.println("OR function");
        testfunction(0,0);
        testfunction(1,0);
        testfunction(0,1);
        testfunction(1,1);
    }
    public static void testfunction(double arg1,double arg2){
        System.out.println("arg1 = "+arg1+"      arg2 = "+arg2);
        neu.setInputsValue(0,arg1);
        neu.setInputsValue(1,arg2);
        System.out.println("result = "+neu.getNeuronOutput());
    }
}
